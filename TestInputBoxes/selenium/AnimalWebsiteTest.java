package selenium;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AnimalWebsiteTest {
	// -----------------------------------
	// Configuration variables
	// -----------------------------------
	// Location of chromedriver file
	final String CHROMEDRIVER_LOCATION = "/Users/macstudent/Desktop/chromedriver";
	// Website we want to test
	final String URL_TO_TEST = "https://www.webdirectory.com/Animals/";
	
	// -----------------------------------
	// Global driver variables
	// -----------------------------------
	WebDriver driver;
	
	@Before
	public void setUp() throws Exception {
		// Selenium setup
		System.setProperty("webdriver.chrome.driver", CHROMEDRIVER_LOCATION);
		driver = new ChromeDriver();
		// 2. go to website
		driver.get(URL_TO_TEST);
	}

	@After
	public void tearDown() throws Exception {
		// After you run the test case, close the browser
		Thread.sleep(5000);
		driver.close();
	}
	
	@Test
	public void testNumberOfLinks() {
		// --------
		// 1. Get all the bulleted links in that section
		List<WebElement> bulletLinks = driver.findElements(By.cssSelector("table+ ul li a"));
		
		System.out.println("Number of links on page: " + bulletLinks.size());

		// 2. Output the links to the screen using System.out.println
		// ------
		// Iterating through the list of links 
		for (int i = 0; i < bulletLinks.size(); i++) {
			// Get the current link
			WebElement link = bulletLinks.get(i);
			
			// Get the link text
			String linkText = link.getText();

			// Get link url
			String linkURL = link.getAttribute("href");

			// Output it to the screen
			System.out.println(linkText + ": " + linkURL);
		}
		
		// 3. Count the number of links in the section
		int actualNumLinks = bulletLinks.size();
		// 4. Check that the number of links = 10
		assertEquals(10, actualNumLinks);

		// --------
	}   
}
