package selenium;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class test {
	WebDriver driver;

	@Before
	public void setUp() throws Exception {
		
System.setProperty("webdriver.chrome.driver", "/Users/macstudent/Desktop/chromedriver");
		

		driver = new ChromeDriver();
		
		driver.get("https://www.seleniumeasy.com/test/basic-first-form-demo.html");
		
		
	}

	@After
	public void tearDown() throws Exception {
		Thread.sleep(2000);
		driver.close();
	}

	@Test
	public void testone() {

		WebElement inputBox = driver.findElement(By.id("user-message"));
		
		inputBox.sendKeys("here is some nonsense");
		
		WebElement showMessageButton = driver.findElement(By.cssSelector("form#get-input button"));
		showMessageButton.click();
		
		WebElement outputbox = driver.findElement(By.id("display"));
		 String actualoutput = outputbox.getText();
		 assertEquals("here is some nonsense",actualoutput);
		 
	}
	@Test
	public void testtwo() {

		
		WebElement inputBox1 = driver.findElement(By.id("sum1"));
		
		inputBox1.sendKeys("25");
		
WebElement inputBox2 = driver.findElement(By.id("sum2"));
		
		inputBox2.sendKeys("50");
		
		WebElement showMessageButton = driver.findElement(By.cssSelector("form#gettotal button"));
		showMessageButton.click();
		
		WebElement outputbox = driver.findElement(By.id("displayvalue"));
		 String actualoutput = outputbox.getText();
		 assertEquals("75",actualoutput);
		 
	}

}
