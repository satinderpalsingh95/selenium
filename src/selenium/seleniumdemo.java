package selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class seleniumdemo {
	
	public static void main(String[] args) throws InterruptedException{
		System.setProperty("webdriver.chrome.driver", "/Users/macstudent/Desktop/chromedriver");
		
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://www.seleniumeasy.com/test/basic-first-form-demo.html");
		
		WebElement inputBox = driver.findElement(By.id("user-message"));
		
		
		inputBox.sendKeys("here is some nonsense");
		
		WebElement showMessageButton = driver.findElement(By.cssSelector("form#get-input button"));
		showMessageButton.click();
		
		WebElement outputbox = driver.findElement(By.id("display"));
		 String actualoutput = outputbox.getText();
		 assertEquals("here is some nonsense",actualoutput);
		 
		Thread.sleep(2000);
		driver.close();
		
	}

	private static void assertEquals(String string, String actualoutput) {
		// TODO Auto-generated method stub
		
	}

	private static By By(String string) {
		// TODO Auto-generated method stub
		return null;
	}

}
